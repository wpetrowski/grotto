package ca.petrowski.grotto
{
  import org.flixel.FlxBasic;
  import org.flixel.FlxGroup;
  import org.flixel.FlxObject;
  import org.flixel.FlxPoint;
  import org.flixel.FlxRect;
  import org.flixel.FlxSprite;
  
  public class Enemy extends FlxSprite
  {
    [Embed(source="../../../../assets/sprites/enemy.png")]
    protected var ImgEnemy:Class;
    
    private var _collidableArea:FlxGroup;
    
    public function Enemy(X:Number, Y:Number, collidableArea:FlxGroup)
    {
      super(X, Y);
      loadGraphic(ImgEnemy, false, true);
      
      _collidableArea = collidableArea;
      
      drag.x = 100;
      drag.y = 50;
      
      acceleration.y = 300;
      
      maxVelocity.x = 30;
      maxVelocity.y = 300;
      
      facing = LEFT;
    }
    
    override public function update():void
    {
      super.update();
      
      if (isTouching(DOWN))
      {
        if (isAtEdge() || justHitWall())
        {
          turnAround();
        }
        
        if (facing == LEFT)
        {
          acceleration.x -= drag.x;
        }
        else if (facing == RIGHT)
        {
          acceleration.x += drag.x;
        }
      }
    }
    
    private function isAtEdge():Boolean
    {
      var bounds:FlxRect = getBoundingBox();
      if (facing == LEFT)
      {
        return isPointEmpty(bounds.left - 1, bounds.bottom + 1);
      }
      else if (facing == RIGHT)
      {
        return isPointEmpty(bounds.right, bounds.bottom + 1);
      }
      return false;
    }
    
    private function justHitWall():Boolean
    {
      return false;
    }
    
    private function turnAround():void
    {
      if (facing == LEFT)
      {
        facing = RIGHT;
      }
      else if (facing == RIGHT)
      {
        facing = LEFT;
      }
      acceleration.x = velocity.x = 0;
    }
    
    private function isPointEmpty(x:Number, y:Number):Boolean
    {
      var testPt:FlxObject = new FlxObject();
      testPt.width = testPt.height = 1;
      return !testPt.overlapsAt(x, y, _collidableArea);
    }
    
    private function getBoundingBox():FlxRect
    {
      return new FlxRect(x, y, width, height);
    }
  }

}