package ca.petrowski.grotto
{
  import org.flixel.FlxGame;
    
  [SWF(width="640", height="480", backgroundColor="#000000")]
  public class GrottoGame extends FlxGame
  {
    public function GrottoGame()
    {
      super(320, 240, MenuState, 2);
      
      forceDebugger = true;
    }
  }
}