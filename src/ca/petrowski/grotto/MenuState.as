package ca.petrowski.grotto 
{
  import org.flixel.FlxButton;
  import org.flixel.FlxState;
  import org.flixel.FlxG;
  import org.flixel.FlxText;
  
  public class MenuState extends FlxState
  {
    override public function create():void
    {
      var titleText:FlxText = new FlxText(0, 60, FlxG.width, "GROTTO");
      titleText.alignment = "center";
      titleText.size = 50;
      add(titleText);
      
      var startButton:FlxButton = new FlxButton(0, 170, "Click to start", startGame);
      startButton.x = FlxG.width / 2 - startButton.width / 2;
      add(startButton);
     
      FlxG.mouse.show();
      
      FlxG.debug = true;
    }
    
    private function startGame():void
    {
      FlxG.switchState(new PlayState());
    }
  }
}