package ca.petrowski.grotto
{
  import flash.display.BitmapData;
  import flash.filters.ShaderFilter;
  import flash.display.Shader;
  import flash.geom.Point;

  import org.flixel.*;
  
  public class PlayState extends FlxState
  {
    [Embed(source="../../../../assets/level_templates/test_level.txt",mimeType="application/octet-stream")]
    private var LevelData:Class;
    
    [Embed(source="../../../../assets/sprites/block.png")]
    private var ImgBlock:Class;

    [Embed(source = "../../../../assets/filters/desat.pbj", mimeType = "application/octet-stream")]
    private var DesatFilterCode:Class;
    
    [Embed(source = "../../../../assets/filters/desat2.pbj", mimeType = "application/octet-stream")]
    private var Desat2FilterCode:Class;
    private var desatShader:Shader;
    
    [Embed(source = "../../../../assets/filters/bw.pbj", mimeType = "application/octet-stream")]
    private var BWFilterCode:Class;
    private var bwShader:Shader;
    private var bwFilter:ShaderFilter;
    
    private var _floor:FlxSprite;
    private var _player:FlxSprite;
    private var _tilemap:FlxTilemap;
    private var _enemies:FlxGroup;
    private var _collidableArea:FlxGroup;
    private var obj_cam:FlxCamera;
    
    private var _dist:int;
    private var _inc:int;
    private var _incwait:int;
    private var _glowgrowspeed:int;
    private var _glowgrowcounter:int;
    
    private var OLD_STYLE:Boolean = false;
    
    override public function create():void
    {
      FlxG.bgColor = 0xff00F0F0;
      
      _enemies = new FlxGroup();
      _collidableArea = new FlxGroup();
      
      createTestLevel();
      
      add(_collidableArea);
      add(_enemies);

      FlxG.camera.setBounds(0, 0, _tilemap.width, _tilemap.height, true);
      FlxG.camera.follow(_player, FlxCamera.STYLE_PLATFORMER);
      
      FlxG.mouse.hide();

      initDesatFilter();
    }

    private function initDesatFilter():void
    {
      if (OLD_STYLE)
      {
        desatShader = new Shader(new DesatFilterCode());

        _dist = 50;
        _inc = 1;
        _glowgrowspeed = 1;
        _glowgrowcounter = 0;
      }
      else
      {
        desatShader = new Shader(new Desat2FilterCode());
        bwShader = new Shader(new BWFilterCode());
        bwFilter = new ShaderFilter(bwShader);
        
        // set up temporary camera so we can try to get just the glow objects in a buffer
        obj_cam = new FlxCamera(0, 0, FlxG.width, FlxG.height);
        obj_cam.copyFrom(FlxG.camera);
        obj_cam.alpha = 0;
        obj_cam.bgColor = 0xff000000;
        FlxG.addCamera(obj_cam);
        
        _collidableArea.setAll("cameras", [FlxG.camera]);
      }
    }
    
    override public function update():void
    {
      super.update();
      FlxG.collide(_collidableArea, _player);
      FlxG.collide(_collidableArea, _enemies);
      
      if (OLD_STYLE)
      {
        updateGlowDistance();
      }
    }

    private function updateGlowDistance():void 
    {
      if (_incwait > 0)
      {
        --_incwait;
      }
      else if (_glowgrowcounter > 0)
      {
        --_glowgrowcounter;
      }
      else
      {
        _dist += _inc;
        _glowgrowcounter = _glowgrowspeed;
        
        if (_dist > 60 || _dist < 35)
        {
          _inc *= -1;
        }
        
        if (_dist < 35)
        {
          _dist = 36;
          _incwait = 60;
        }
      }
    }
    
    override public function draw():void
    {
      super.draw();
      
      if (OLD_STYLE)
      {
        applyOldDesat();
      }
      else
      {
        applyNewDesat();
      }
    }

    private function applyOldDesat():void
    {
      desatShader.data.pt.value = [_player.getMidpoint().x - FlxG.camera.scroll.x, _player.getMidpoint().y - FlxG.camera.scroll.y];
      desatShader.data.dist.value = [_dist];
      
      var desatFilter:ShaderFilter = new ShaderFilter(desatShader);
      
      FlxG.camera.buffer.applyFilter(FlxG.camera.buffer, FlxG.camera.buffer.rect, new Point(), desatFilter);
    }
    
    private function applyNewDesat():void
    {
      var bwData:BitmapData = new BitmapData(obj_cam.buffer.width, obj_cam.buffer.height, true, 0xFF000000);
      bwData.applyFilter(obj_cam.buffer, obj_cam.buffer.rect, new Point(), bwFilter);
     
      desatShader.data.bg.input = FlxG.camera.buffer;
      desatShader.data.objs.input = bwData;
      var desatFilter:ShaderFilter = new ShaderFilter(desatShader);
      
      FlxG.camera.buffer.applyFilter(FlxG.camera.buffer, FlxG.camera.buffer.rect, new Point(), desatFilter);
    }
    
    private function createTestLevel():void
    {
      var csv:String = (new LevelData()).toString();
      
      _tilemap = new FlxTilemap();
      _tilemap.loadMap(csv, ImgBlock, 8, 8, FlxTilemap.OFF);
      
      _collidableArea.add(_tilemap);
      
      _player = new Player(50, FlxG.height - 40);
      add(_player);
      
      var _enemy:Enemy = new Enemy(120, 150, _collidableArea);
      _enemies.add(_enemy);
    }
  }
}