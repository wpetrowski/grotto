package ca.petrowski.grotto
{
  import org.flixel.FlxSprite;
  import org.flixel.FlxG;
  
  public class Player extends FlxSprite
  {
    [Embed(source="../../../../assets/sprites/player.png")]
    protected var ImgPlayer:Class;
    
    public function Player(X:Number, Y:Number)
    {
      super(X, Y);
      loadGraphic(ImgPlayer, false, true);
      
      drag.x = 350;
      drag.y = 50;
      
      acceleration.y = 500;
      
      maxVelocity.x = 125;
      maxVelocity.y = 200;
    }
    
    override public function update():void
    {
      super.update();
      
      acceleration.x = 0;
      if (FlxG.keys.LEFT)
      {
        acceleration.x -= drag.x;
        facing = LEFT;
      }
      else if (FlxG.keys.RIGHT)
      {
        acceleration.x += drag.x;
        facing = RIGHT;
      }
      if (FlxG.keys.justPressed("UP") && isTouching(DOWN))
      {
        velocity.y = -200;
      }
    
    }
  
  }

}